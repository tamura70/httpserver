package httpserver

import java.io.File
import com.sun.net.httpserver.{HttpExchange, HttpHandler, HttpServer}

class Server(htmlDir: String, port: Int, debug: Boolean = false) {
  val htmlPath = "/html"
  val commandPath = "/command"

  def urlDecode(str: String): String = {
    val charset = java.nio.charset.StandardCharsets.UTF_8.name
    java.net.URLDecoder.decode(str, charset)
  }

  class HtmlHandler extends HttpHandler {
    val buffSize = 1024

    def handle(exchange: HttpExchange): Unit = synchronized {
      val out = exchange.getResponseBody()
      try {
        val path0 = exchange.getHttpContext().getPath()
        val path1 = exchange.getRequestURI().getPath()
        val path = urlDecode(htmlDir + path1.substring(path0.length()))
        if (debug) {
          println(s"HtmlHandler: $path")
        }
        val file = new File(path)
        if (file.isFile()) {
          val htmlDirPath = (new File(htmlDir)).getAbsolutePath + "/"
          if (! file.getAbsolutePath.startsWith(htmlDirPath)) { // 不要?
            throw new java.io.IOException(s"Illegal path $path")
          }
          exchange.sendResponseHeaders(200, 0)
          val in = new java.io.FileInputStream(file)
          val buff = new Array[Byte](buffSize)
          while (in.available() > 0) {
            val length = in.read(buff)
            out.write(buff, 0, length)
          }
        } else {
          val str = "File Not Found " + exchange.getRequestURI()
          exchange.sendResponseHeaders(404, 0)
          out.write(str.getBytes())
        }
      } catch {
        case e: Exception => {
          val str = "Error " + e.getMessage()
          exchange.sendResponseHeaders(404, 0)
          out.write(str.getBytes())
        }
      }
      out.close()
    }
  }

  class CommandHandler extends HttpHandler {
    def query2map(str: String): Map[String,String] =
      if (str == null) {
        Map.empty
      } else {
        str.split("&").map(_.split("=") match {
          case Array(k) => k -> ""
          case Array(k,v) => k -> urlDecode(v)
        }).toMap
      }
    def handle(exchange: HttpExchange): Unit = synchronized {
      val out = exchange.getResponseBody()
      try {
        val path0 = exchange.getHttpContext().getPath()
        val path1 = exchange.getRequestURI().getPath()
        val path = path1.substring(path0.length())
        val params = query2map(exchange.getRequestURI().getQuery())
        if (debug) {
          println(s"CommandHandler: $path $params")
        }
        Command.exec(path, params) match {
          case None => {
            val str = s"Command Not Found $path"
            exchange.sendResponseHeaders(404, 0)
            out.write(str.getBytes())
          }
          case Some(str) => {
            exchange.getResponseHeaders().add("Content-Type", "application/json")
            exchange.sendResponseHeaders(200, 0)
            out.write(str.getBytes())
          }
        }
      } catch {
        case e: Exception => {
          val str = "Error " + e.getMessage()
          exchange.sendResponseHeaders(404, 0)
          out.write(str.getBytes())
        }
      }
      out.close()
    }
  }

  def start(): HttpServer = {
    val httpServer = HttpServer.create(new java.net.InetSocketAddress(port), 0)
    httpServer.createContext(htmlPath, new HtmlHandler())
    httpServer.createContext(commandPath, new CommandHandler())
    httpServer.start()
    if (debug) {
      println(s"Running HttpServer on $port")
    }
    httpServer
  }
}

object Server {
  def main(args: Array[String]): Unit = {
    val server = new Server("html", 8080, true)
    server.start()
  }
}
