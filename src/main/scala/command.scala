package httpserver

/*
   https://github.com/json4s/json4s
 */

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

object Command {
  /* https://stackoverflow.com/questions/23348480/json4s-convert-type-to-jvalue */
  def encodeJson(src: AnyRef): JValue = {
    import org.json4s.{ Extraction, NoTypeHints }
    import org.json4s.JsonDSL.WithDouble._
    import org.json4s.jackson.Serialization
    implicit val formats = Serialization.formats(NoTypeHints)
    Extraction.decompose(src)
  }
 
  def handler(path: String, params: Map[String,String]): JValue = path match {
    case "/date" => {
      val fmt = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
      val str = fmt.format(new java.util.Date)
      JString(str)
    }
    case "/draw" => {
      params.get("n") match {
        case Some("1") => {
          JString("digraph { a -> b; }")
        }
        case _ => JNull
      }
    }
    case "/items" => {
      encodeJson(Item.items)
    }
    case "/item" => {
      params.get("id") match {
        case None => JNull
        case Some(id) => encodeJson(Item.get(id))
      }
    }
    case _ => {
      JNull
    }
  }
  def exec(path: String, params: Map[String,String]): Option[String] = {
    val result: JValue = handler(path, params)
    if (result == JNull) {
      None
    } else {
      val json = 
        ("path" -> path) ~
        ("params" -> params) ~
        ("result" -> result)
      Some(compact(render(json)))
    }
  }
}

