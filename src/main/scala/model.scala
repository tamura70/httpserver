package httpserver

case class Item(id: String, name: String)

object Item {
  def items: Map[String,Item] = Map(
    "001" -> Item("001", "coffee"),
    "002" -> Item("002", "milk"),
    "003" -> Item("003", "tea"),
  )

  def get(id: String): Option[Item] =
    items.get(id)
}
