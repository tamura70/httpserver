# README

## 実行方法

1. `sbt run`
2. Open
   - <http://localhost:8080/html/httpserver.html>
   - <http://localhost:8080/html/draw.html>

## APIテスト

- <http://localhost:8080/command/date>
- <http://localhost:8080/command/draw?n=1>
- <http://localhost:8080/command/items>
- <http://localhost:8080/command/item?id=001>
- <http://localhost:8080/command/item?id=999>

## リンク

- [GitHub: json4s](https://github.com/json4s/json4s)
- [GitHub: viz](https://github.com/mdaines/viz.js)
- [GitHub: d3-graphviz](https://github.com/magjac/d3-graphviz)
- [D3.js](https://d3js.org/)
- [Java: Java HttpSever](https://docs.oracle.com/javase/jp/8/docs/jre/api/net/httpserver/spec/com/sun/net/httpserver/HttpServer.html)
- [IT用語辞典: URLパラメータ](https://e-words.jp/w/URL%E3%83%91%E3%83%A9%E3%83%A1%E3%83%BC%E3%82%BF.html)
- [IT用語辞典: URLエンコード](https://e-words.jp/w/URL%E3%82%A8%E3%83%B3%E3%82%B3%E3%83%BC%E3%83%89.html)
- [IT用語辞典: MIMEタイプ](https://e-words.jp/w/MIME%E3%82%BF%E3%82%A4%E3%83%97.html)
- [IT用語辞典: HTTPステータス](https://e-words.jp/w/%E3%82%B9%E3%83%86%E3%83%BC%E3%82%BF%E3%82%B9%E3%82%B3%E3%83%BC%E3%83%89.html#Section_HTTP%E3%82%B9%E3%83%86%E3%83%BC%E3%82%BF%E3%82%B9%E3%82%B3%E3%83%BC%E3%83%89)
- [IT用語辞典: JSON](https://e-words.jp/w/JSON.html)
- [IT用語辞典: Ajax](https://e-words.jp/w/Ajax.html)
- [IT用語辞典: JavaScript](https://e-words.jp/w/JavaScript.html)
- [Mozilla: JavaScriptチュートリアル](https://developer.mozilla.org/ja/docs/Web/JavaScript)
