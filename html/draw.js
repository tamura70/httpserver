function drawButton() {
    sendRequest("/command/draw?n=1", function (http) {
        var result = http.responseText;
        if (result != "") {
            var json = JSON.parse(result)
            document.getElementById("graph1").innerHTML = "";
            d3.select("#graph1").graphviz()
                .fade(false)
                .renderDot(json["result"]);
        }
    });
}
