function escapeHTML(str) {
    return str.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll('"', "&quot;");
}

function dateButton() {
    sendRequest("/command/date", function (http) {
        var result = http.responseText;
        if (result != "") {
            var json = JSON.parse(result)
            var dateNode = document.getElementById("date");
            dateNode.innerHTML = escapeHTML(json["result"]);
        }
    });
}

function sendRequest(path, callback) {
    var url = window.location.protocol + "//" + window.location.host + path;
    var http = new XMLHttpRequest();
    http.overrideMimeType("text/plain; charset=UTF-8");
    http.open("GET", url, true);
    if (callback) {
	http.onreadystatechange = function () {
            if (http.readyState != 4) {
	        return;
            }
            if (http.status != 200) {
	        alert("Server is not responding : " + http.status);
	        return;
            }
            callback(http);
        };
    }
    http.send(null);
}
